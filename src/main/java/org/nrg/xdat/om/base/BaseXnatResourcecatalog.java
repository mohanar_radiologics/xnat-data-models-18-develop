/*
 * xnat-data-models: org.nrg.xdat.om.base.BaseXnatResourcecatalog
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xdat.om.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.GZIPInputStream;

import org.nrg.action.ServerException;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.bean.CatEntryBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.reader.XDATXMLReader;
import org.nrg.xdat.model.CatCatalogI;
import org.nrg.xdat.model.CatEntryI;
import org.nrg.xdat.om.base.auto.AutoXnatResourcecatalog;
import org.nrg.xft.ItemI;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.FileUtils;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.utils.CatalogUtils;

import javax.annotation.Nullable;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseXnatResourcecatalog extends AutoXnatResourcecatalog {

	public BaseXnatResourcecatalog(ItemI item)
	{
		super(item);
	}

	public BaseXnatResourcecatalog(UserI user)
	{
		super(user);
	}

	private Map<File, CatEntryI> filesMap = null;

	/*
	 * @deprecated Use BaseXnatResourcecatalog(UserI user)
	 **/
	public BaseXnatResourcecatalog()
	{}

	public BaseXnatResourcecatalog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

    /**
     * Returns ArrayList of java.io.File objects
     * @return
     */
    public ArrayList getCorrespondingFiles(String rootPath)
    {
        if (files==null)
        {
			getCorrespondingFilesWithCatEntries(rootPath);
        	files = new ArrayList<>(filesMap.keySet());
        }
        return files;
    }

    @Override
	public Map<File, CatEntryI> getCorrespondingFilesWithCatEntries(String rootPath) {
    	if (filesMap == null) {
			filesMap = new HashMap<>();
			final CatalogUtils.CatalogData catalogData;
			try {
				catalogData = CatalogUtils.CatalogData.getOrCreate(rootPath, this, null);
				for (CatEntryI entry : catalogData.catBean.getEntries_entry()) {
					File temp = CatalogUtils.getFile(entry, catalogData.catPath, catalogData.project);
					if (temp != null) {
						filesMap.put(temp, entry);
					}
				}
			} catch (ServerException e) {
				logger.error("Unable to read catalog", e);
			}
		}
    	return filesMap;
	}

    public void clearFiles() {
        files = null;
    }
	

    /**
     * Returns ArrayList of java.lang.String objects
     * @return
     */
    public ArrayList getCorrespondingFileNames(String rootPath)
    {
        if (fileNames==null)
        {
            fileNames = new ArrayList();
            ArrayList files = getCorrespondingFiles(rootPath);
            for (int i=0;i<files.size();i++){
                File f = (File)files.get(i);
                fileNames.add(f.getName());
            }
        }
        return fileNames;
    }

    /**
     * Appends this path to the enclosed URI or path variables.
     * @param root
     */
    public void prependPathsWith(String root){
        if (!FileUtils.IsAbsolutePath(this.getUri())){
            try {
                    this.setUri(root + this.getUri());
            } catch (Exception e) {
                logger.error("",e);
            }
        }
    }

	@Deprecated
	public File getCatalogFile(String rootPath){
		return CatalogUtils.getCatalogFile(rootPath, this);
	}

	@Deprecated
	public CatCatalogBean getCatalog(String rootPath){
		return CatalogUtils.getCatalog(rootPath, this, null);
	}

	@Deprecated
	public CatCatalogBean getCatalog(String rootPath, String project){
		return CatalogUtils.getCatalog(rootPath, this, project);
	}

    public static void backupEntry(CatalogUtils.CatalogData catalogData, UserI user, EventMetaI c, String timestamp)
			throws FileNotFoundException, IOException{
		for(CatEntryI entry: catalogData.catBean.getEntries_entry()){
			final File f = CatalogUtils.getFile(entry, catalogData.catPath, catalogData.project);
			if (f == null) {
				continue;
			}
			final File newFile=FileUtils.CopyToHistory(f,timestamp);
			entry.setUri(newFile.getAbsolutePath());
			((CatEntryBean)entry).setModifiedtime(EventUtils.getEventDate(c, false));
			if(user!=null){
				entry.setModifiedby(user.getUsername());
			}

			if(c!=null && c.getEventId()!=null){
				entry.setModifiedeventid(Integer.valueOf(c.getEventId().intValue()));
			}
		}
    }
    
    public void backupToHistory(String rootPath, String project, UserI user, EventMetaI c) throws Exception{
    	String timestamp = EventUtils.getTimestamp(c);
		CatalogUtils.CatalogData catalogData = CatalogUtils.CatalogData.getOrCreate(rootPath, this, project);
		CatalogUtils.CatalogData historyCatalogData = new CatalogUtils.CatalogData(
				FileUtils.BuildHistoryFile(catalogData.catFile, timestamp),
				catalogData.project);
		backupEntry(catalogData, user, c, timestamp);
		CatalogUtils.writeCatalogToFile(historyCatalogData);
    }
    


    public void deleteWithBackup(String rootPath, String project, UserI user, EventMetaI c) throws Exception{
    	if(CatalogUtils.maintainFileHistory()){
    		backupToHistory(rootPath, project, user, c);
    	}
			
    	deleteFromFileSystem(rootPath, project);
    }

    public void deleteFromFileSystem(String rootPath, String project){
    	super.deleteFromFileSystem(rootPath, project);
    	
    	final File f = this.getCatalogFile(rootPath);	

    	if (f.exists()){
    		try {
    			FileUtils.MoveToCache(f);
    			if(FileUtils.CountFiles(f.getParentFile(),true)==0){
    				FileUtils.DeleteFile(f.getParentFile());
    			}
    		} catch (FileNotFoundException e) {
    			logger.error("",e);
    		} catch (IOException e) {
    			logger.error("",e);
    		}
    	}
    }
    
    public int entryCount =0;
    @Deprecated
    public boolean formalizeCatalog(CatCatalogI cat, String catPath, String project, UserI user, EventMetaI now){
    	return CatalogUtils.formalizeCatalog(cat, catPath, project, user, now);
    }

    @Deprecated
    public CatCatalogBean getCleanCatalog(String rootPath, String project, boolean includeFullPaths,UserI user, EventMetaI c){
    	return CatalogUtils.getCleanCatalog(project, rootPath, this, includeFullPaths,user,c);
    }
    
    public void clearCountAndSize() {
    	count = null;
    	size = null;
    }

    Integer count = null;
    public Integer getCount(String rootPath){
        if (count ==null){
            long sizeI = 0;
            int countI = 0;
            Iterator files = this.getCorrespondingFiles(rootPath).iterator();
            while (files.hasNext()){
                File f = (File)files.next();
               
                if (f!=null && f.exists() && !f.getName().endsWith("catalog.xml")){
                    countI++;
                    sizeI+=f.length();
                }
            }

            size = new Long(sizeI);
            count = new Integer(countI);
        }
        return count;
    }

    Long size = null;
    public long getSize(String rootPath){
        if (size ==null){
            long sizeI = 0;
            int countI = 0;
            Iterator files = this.getCorrespondingFiles(rootPath).iterator();
            while (files.hasNext()){
                File f = (File)files.next();
                if (!f.getName().endsWith("catalog.xml")){
                    countI++;
                    sizeI+=f.length();
                }
            }

            size = new Long(sizeI);
            count = new Integer(countI);
        }
        return size.longValue();
    }
    
    public void moveTo(File newSessionDir, String existingSessionDir, String rootPath, @Nullable String currentProject,
					   @Nullable String destinationProject, UserI user, EventMetaI ci) throws IOException, Exception {
    	String uri = this.getUri();
    	
    	String relativePath=null;
    	if(existingSessionDir!=null && uri.startsWith(existingSessionDir)){
    		relativePath=uri.substring(existingSessionDir.length());
    	}else{
    		if(FileUtils.IsAbsolutePath(uri)){
    			if(uri.indexOf("/")>0){
    				relativePath=uri.substring(uri.indexOf("/")+1);
    			}else if(uri.indexOf("\\")>0){
    				relativePath=uri.substring(uri.indexOf("\\")+1);
    			}else{
    				relativePath=uri;
    			} 
    		}else{
    			relativePath=uri;
    		}
    	}
    	
    	File newFile = new File(newSessionDir,relativePath);
    	File parentDir=newFile.getParentFile();
    	if(!parentDir.exists())
    	{
    		parentDir.mkdirs();
    	}

    	File catalog =this.getCatalogFile(rootPath);
    	
    	InputStream fis = new FileInputStream(catalog);
        if (catalog.getName().endsWith(".gz"))
        {
            fis = new GZIPInputStream(fis);
        }
        XDATXMLReader reader = new XDATXMLReader();
        BaseElement base = reader.parse(fis);

        if (base instanceof CatCatalogBean){
        	moveCatalogEntries((CatCatalogBean) base, catalog.getParent(), newFile.getParent(), currentProject,
					destinationProject);
        }
        
        try {
			FileWriter fw = new FileWriter(catalog);
			base.toXML(fw, true);
			fw.close();
		} catch (IOException e) {
			logger.error("",e);
		}
		
    	FileUtils.MoveFile(catalog, newFile, true, true);
    	
    	this.setUri(newFile.getAbsolutePath());
    	SaveItemHelper.authorizedSave(this,user, true, false,ci);
    }
    
    public void moveCatalogEntries(CatCatalogI cat, String existingRootPath, String newRootPath,
								   @Nullable String currentProject, @Nullable String destinationProject)
			throws IOException {

    	if (currentProject == null && cat instanceof CatCatalogBean) {
    		currentProject = CatalogUtils.getCatalogProject((CatCatalogBean) cat);
		}

    	for (CatEntryI entry : cat.getEntries_entry()) {
			String uri = entry.getUri().replaceFirst("^file://","");
			String relativePath = uri;
    		File existingLocation = CatalogUtils.getFile(entry, existingRootPath, currentProject);
    		if (existingLocation == null || !existingLocation.exists()) {
    			logger.error("Unable to retrieve file corresponding to catalog entry URI=" + uri +
						" in catalog " + newRootPath + "/" + cat.getId());
    			continue;
			}

    		// Handle URLs or absolute paths
    		if (FileUtils.IsAbsolutePath(relativePath)) {
    			if (FileUtils.IsUrl(uri, true)) {
    				relativePath = entry.getId();
				} else {
					if (uri.startsWith(existingRootPath)) {
						relativePath = uri.substring(existingRootPath.length());
					} else {
						// "Fudge" a relative path
						if (uri.indexOf("/") > 0) {
							relativePath = uri.substring(uri.indexOf("/") + 1);
						} else if (uri.indexOf("\\") > 0) {
							relativePath = uri.substring(uri.indexOf("\\") + 1);
						}
					}
					entry.setUri(relativePath);
					entry.setId(relativePath);
				}
    		}

    		// Move the file locally
    		File newFile = new File(newRootPath, relativePath);
        	File parentDir = newFile.getParentFile();
        	if (!parentDir.exists()) {
        		parentDir.mkdirs();
        	}
          	FileUtils.MoveFile(existingLocation, newFile, true, true);
    	}
    	
    	for(CatCatalogI subset: cat.getSets_entryset()){
    		moveCatalogEntries(subset, existingRootPath, newRootPath, currentProject, destinationProject);
    	}

    	// reset project in catalog
		if (destinationProject != null && !destinationProject.equals(currentProject) && cat instanceof CatCatalogBean) {
			CatalogUtils.setCatalogProject((CatCatalogBean) cat, destinationProject);
		}
    }
}
