/*
 * xnat-data-models: org.nrg.xdat.om.base.BaseXnatAbstractprojectasset
 * XNAT http://www.xnat.org
 * Copyright (c) 2019, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.om.base;

import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.auto.AutoXnatAbstractprojectasset;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * This class and the clara:abstractProjectAsset data type are based on an implementation in the
 * <b>features/proj-assessors</b> branch of <b>xnat-data-models</b>. Once that's been merged into
 * the main <b>develop</b> branch, this should be removed and the <b>clara:trainConfig</b> data
 * type should be migrated to extend the <b>xnat:abstractProjectAsset</b> data type instead.
 */
public abstract class BaseXnatAbstractprojectasset extends AutoXnatAbstractprojectasset {
    public BaseXnatAbstractprojectasset(final ItemI item) {
        super(item);
    }

    public BaseXnatAbstractprojectasset(final UserI user) {
        super(user);
    }

    /**
     * @deprecated Use BaseAbstractProjectAsset(UserI user)
     */
    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public BaseXnatAbstractprojectasset() {}

    public BaseXnatAbstractprojectasset(final Hashtable properties, final UserI user) {
        super(properties, user);
    }

    /**
     * Returns a list of the {@link XnatSubjectdata#getId() subject IDs} associated with this project assessor, if any. This provides
     * a faster way to get the list of subjects compared to {@link #getSubjects_subject()}, as this method doesn't retrieve the full
     * {@link XnatSubjectdata subject object} but just the IDs instead.
     *
     * @return A list of IDs of subjects associated with this assessor or an empty list if no subjects are associated with the assessor.
     */
    @SuppressWarnings("unused")
    public List<String> getSubjectIds() {
		/*
		final XFTTable table = XFTTable.Execute("SELECT id FROM xnat_subjectdata WHERE project='" + getId() + "' GROUP BY element_name;", getDBName(), null);
		table.resetRowCursor();
		while (table.hasMoreRows()) {
			final Object[] row      = table.nextRow();
			final Long     count    = (Long) row[0];
			final String   initial = (String) row[1];
			try {
				final SchemaElement schemaElement = SchemaElement.GetElement(initial);
				hash.put(schemaElement.getProperName(), count);
			} catch (XFTInitException e) {
				logger.error("An error occurred accessing XFT while trying to create a new element security entry for an item of type {}" + SCHEMA_ELEMENT_NAME, e);
				hash.put(initial, count);
			} catch (ElementNotFoundException e) {
				logger.error("Couldn't find the element " + e.ELEMENT + " while trying to create a new element security entry for an item of type " + SCHEMA_ELEMENT_NAME, e);
				hash.put(initial, count);
			}
		}
    	*/
        return Collections.emptyList();
    }

    /**
     * Returns a list of the {@link XnatExperimentdata#getId() subject IDs} associated with this project assessor, if any. This provides
     * a faster way to get the list of subjects compared to {@link #getExperiments_experiment()}, as this method doesn't retrieve the full
     * {@link XnatExperimentdata subject object} but just the IDs instead.
     *
     * @return A list of IDs of experiments associated with this assessor or an empty list if no experiments are associated with the assessor.
     */
    @SuppressWarnings("unused")
    public List<String> getExperimentIds() {
		/*
		final XFTTable table = XFTTable.Execute("SELECT id FROM xnat_subjectdata WHERE project='" + getId() + "' GROUP BY element_name;", getDBName(), null);
		table.resetRowCursor();
		while (table.hasMoreRows()) {
			final Object[] row      = table.nextRow();
			final Long     count    = (Long) row[0];
			final String   initial = (String) row[1];
			try {
				final SchemaElement schemaElement = SchemaElement.GetElement(initial);
				hash.put(schemaElement.getProperName(), count);
			} catch (XFTInitException e) {
				logger.error("An error occurred accessing XFT while trying to create a new element security entry for an item of type {}" + SCHEMA_ELEMENT_NAME, e);
				hash.put(initial, count);
			} catch (ElementNotFoundException e) {
				logger.error("Couldn't find the element " + e.ELEMENT + " while trying to create a new element security entry for an item of type " + SCHEMA_ELEMENT_NAME, e);
				hash.put(initial, count);
			}
		}
    	*/
        return Collections.emptyList();
    }
}
